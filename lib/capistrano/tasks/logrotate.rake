namespace :logrotate do
  include Capistrano::Util

  set :nginx_log_dir, -> { "/var/log/nginx/#{application}" }

  desc "upload logrotate config and create old dir"
  task :setup do
    on roles(:app), in: :sequence do
      output_path = "/etc/logrotate.d/#{application}"
      root_upload_file(File.join(templates_path, "logrotate", "logrotate.erb"), output_path)
      create_writable_dir "#{fetch(:nginx_log_dir)}/old"
    end
  end
end
