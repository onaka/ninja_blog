---
title: RubyKaigi 2017 でどんな発表をしたか
date: 2017/09/21T00:00:00 +09:00
tags: rubykaigi
---


RubyKaigi 2017 でどんな発表をしたか
================================

## 発表スライド

荷物とともに PC 送っちゃったのであとで貼ります :sweat_drops:

ほぼ同内容のテキストはこちらの 4 記事です。

* [RESTful API のおさらい](/2017/09/21/review_of_restful_api)
* [Rails での JSON API 実装まとめ](/2017/09/21/history_of_json_api_with_ruby)
* [スキーマファースト開発](/2017/09/21/schema_first_development)
* [The NEXT of REST](/2017/09/21/the_next_of_rest)

## 発表時の twitter での反応

togetter にまとめておきました。

[API Development in 2017 - RubyKaigi 2017 #rubykaigi - Togetterまとめ](https://togetter.com/li/1153072)

<script src="https://s.togetter.com/static/web/js/parts.js"></script><script>tgtr.ListWidget({id:'1153072',url:'https://togetter.com/',width:'320px',height:'240px'});</script>


Proposal
--------------------------------

事前資料の公開が推奨されていたので、CFP に出した内容も置いておきます。

事後になってしまい申し訳ありません。

### Title

API Development in 2017

公開されてから思ったんだけど、RubyKaigi は ruby の API の話が多いので **web** を付けるべきでしたね。

### Abstract

Summarize "How to develop API server efficiently."

This talk will talk while looking back on the history like

* Why REST (RESTful API) was born?
* The world has became to need Native client / Web front-end
* API documentation tool are widely used
   * API Blueprint, Swagger, RAML, JSON Hyper-Schema
   * Schema driven development
* API Query Language (GraphQL)'s birth

And I talk about the library concept and code that we implemented as necessary.
There were many challenges such as how to communicate at the interface boundary, how to implement without any mistakes, etc.

### Details

このトークを通じて、API 実装時に考えることのとっかかりを掴んで帰ってもらいます。

ネイティブクライアントや Web フロントエンドの隆盛により、今までは 1 チームで加工した HTML を返せば良かった Web 系の開発は Android や iOS, JavaScript を操る別開発チームと連携しないとユーザに価値を届けられない状態になりました。別チームとの連携ではインタフェースの定義やテストが必要になりますが、そのためのツール群が一般に流通しているとはまだ言い難い状態だと考えています。

このトークでは、歴史を振り返ることで「どのような問題があり、それがどういう技術で解決されたのか」を追っていきます。

* ドキュメント自動生成
* クライアント自動生成
* request validator 自動生成
* response validator 自動生成

の導入が数手で終わる状態がスタートラインで、その先にも様々な問題があり、それらをどう解決していったのかを示すことで、API 開発のツラみの勘所と、なぜそれがツラいのか（＝開発を始める前に考えなければいけないぐらい大事なことなのか）を総括します。

また、効率的に API を作るために自分たちでも各種 gem を作ってきているので、それらが何を解決しているのかの紹介を行います。

* スキーマ定義のみ行うことで、自動的に JSON や messagePack にシリアライズする gem
  * view 層が不要になります
* [JSON References](https://tools.ietf.org/html/draft-pbryan-zyp-json-ref-03) / [JSON Pointer](https://tools.ietf.org/html/rfc6901) を YAML で実装した gem
  * YAML 内から別ファイルの YAML を参照できるようになります
* JSON Schema と OpenAPI の差を埋める gem
  * 例えば nullable の定義の仕方が違うのですが、いい感じにします

歴史を伝え、これらの必要に駆られて実装した gem の紹介をすることで、参加者が業務で「今よりもっと楽になれそう」という希望を持ち帰ることができると考えています。

### Pitch

ドリコムでは v1.0 が出たころから Rails を使っており、Rails v2.0 で RESTful を学び、Grape のような API フレームワークや RABL, jbuilder 等の JSON テンプレートライブラリ、ActiveModel::Serializer、JSON Schema によるスキーマファーストの開発体制等々を導入してきました。また、スマートフォンゲームではエンジニアチームが 20 名を超えることも多く、比較的大規模な API 開発を行っています。

歴史を体験で語ることができること、現在も業務の根幹として改善し続けていることから、私はこのトークに自分がふさわしいと考えています。このトークは RubyKaigi に来る人の業務の中心であろう Web 系の開発を、更に生産的に、ツラみを減らして楽しく開発できるようにするためのものです。
