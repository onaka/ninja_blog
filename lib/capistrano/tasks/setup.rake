namespace :deploy do
  include Capistrano::Util

  desc "Setup directries and parmission for app"
  task :setup do
    on roles(:app), in: :sequence do
      create_dir fetch(:deploy_to), mode: "2775", owner: fetch(:www_user), group: fetch(:app_group)

      execute :touch, revision_log

      invoke "deploy:check"

      invoke "nginx:setup"
      invoke "logrotate:setup"
    end
  end
end
