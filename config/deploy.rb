# config valid only for current version of Capistrano
lock "3.14.1"

set :application, "blog"

# -----------------------------------------------------------
# Repository setting
# -----------------------------------------------------------
set :scm, :middleman

set :deploy_to, "/var/www/#{fetch(:application)}"
set :keep_releases, 5

set :app_group, "onaka"

# -----------------------------------------------------------
# SSH setting
# -----------------------------------------------------------
set :pty, true
set :user, -> { ENV["CAP_USER"] || ENV["USER"] || local_user }
set :ssh_options, {
  user: fetch(:user),
  port: 10022,
  forward_agent: true,
}
SSHKit.config.umask = "002"

# -----------------------------------------------------------
# web setting
# -----------------------------------------------------------
set :www_user, "www-data"
set :www_group, "www-data"

# -----------------------------------------------------------
# tasks
# -----------------------------------------------------------
namespace :deploy do
  after :restart, :clear_cache do
    on roles(:web), in: :groups, limit: 3, wait: 10 do
      # Here we can do anything such as:
      # within release_path do
      #   execute :rake, 'cache:clear'
      # end
    end
  end
end
