---
title: Rails Developers Meetup 2017 で RSpec しぐさについて話した
date: 2017/12/13T03:24:54
ogp:
  og:
    image:
      "": "/images/2017/12/13/test_driven_development_by_example.jpg"
      type: "image/jpeg"
---

Rails Developers Meetup 2017 で RSpec しぐさについて話した
================================

Rails Developers Meetup の年末拡大版である、[Rails Developers Meetup 2017](https://railsdm.github.io/) で発表させていただきました。

RSpec をどう書いていくと良いのかの指針、みたいな話です。資料はこちら

<iframe src="//www.slideshare.net/slideshow/embed_code/key/oF8wYt9l2w96do" width="595" height="485" frameborder="0" marginwidth="0" marginheight="0" scrolling="no" style="border:1px solid #CCC; border-width:1px; margin-bottom:5px; max-width: 100%;" allowfullscreen> </iframe> <div style="margin-bottom:5px"> <strong> <a href="//www.slideshare.net/takafumionaka/rspec-83693226" title="RSpecしぐさ" target="_blank">RSpecしぐさ</a> </strong> from <strong><a href="https://www.slideshare.net/takafumionaka" target="_blank">Takafumi ONAKA</a></strong> </div>

スライドを作るにあたって考えたこと
--------------------------------

「5 分では RSpec の **こう書くべき** という話にたどり着けない」が最初の山でした。
考えてみれば [第 1 回 Rails Developers Meetup](https://rails-developers-meetup.connpass.com/event/55363/) の [willnet さんの話](http://blog.willnet.in/entry/2017/05/19/120536) が同じ題材 (RSpec の書き方) で、35 分枠だったので当然ですね。

そこで伝えたいことを絞ることにしました。どうせ聴衆は

* [Rubyist Magazine - スはスペックのス 【第 1 回】 RSpec の概要と、RSpec on Rails (モデル編)](http://magazine.rubyist.net/?0021-Rspec)
* [RSpec の入門とその一歩先へ - t-wadaの日記](http://d.hatena.ne.jp/t-wada/20100228/p1)
* [Rubyist Magazine - 改めて学ぶ RSpec](http://magazine.rubyist.net/?0035-RSpecInPractice)
* [RSpecによるユニットテストの書き方 — recompile.net](https://recompile.net/posts/how-to-write-unit-test-with-rspec.html)
* [Introduction · rspec-style-guide](https://willnet.gitbooks.io/rspec-style-guide/content/)
* [Better Specs { rspec guidelines with ruby }](http://www.betterspecs.org/)
* [Everyday Rails Testing with… by Aaron Sumner [PDF/iPad/Kindle]](https://leanpub.com/everydayrailsrspec)

辺りの「綺麗な RSpec の書き方」は見ているだろうから、それらと、ぼくのかんがえたさいきょうの RSpec の書き方との「違い」にフォーカスすれば良いやと。
(でも古い記事が多いので、実はあまり見てないかもしれないなーと思ったのでスライド末尾に URL を載せたのでした)

というわけで伝えたいことを 3 つに絞りました。

* BDD って何？
* Given-When-Then という構造化技法
* `should` 記法という破れた夢

です。


BDD って何？
--------------------------------

スライド中にも書きましたが、

> BDD とは、TDD を「テスト」を使わないように Dan North が発明した「言い換え」

です。

[スはスペックのス](http://magazine.rubyist.net/?0021-Rspec) においても

> ###FAQ:「RSpec って、要は Test::Unit でやっていることを別の書き方にしただけでは？」
>
> この FAQ への短い答えはイエスです。 しかし、その「書き方」が人の思考を変えてしまうことを、Rubyist の皆さんはよく知っている と思います。

とあります。

BDD が定義した語彙を使うと自然に仕様を表現できるようになったので、
テスト駆動開発は xUnit の一歩先の世界にたどり着けたのだと考えています。

まずはこの衝撃を伝えたかった。


Given-When-Then という構造化技法
--------------------------------

RSpec/Cucumber の中で発明された語彙の中に [GivenWhenThen](https://martinfowler.com/bliki/GivenWhenThen.html) というものがあります。
普段の開発の中では Cucumber のシナリオを書いているときにしか出てきませんが、これが BDD の、振る舞いを記述するための語彙だと僕は認識しています。

振る舞いを記述するために Given-When-Then という構造化技法があり、これが Arrange-Act-Assert とは違う語彙だからこそ生み出せる書き味というものがあります。スライド中に言及したネストや順序を問わない部分がまさにそれですね。(参考: [Given-When-Then - 日々ごちゃごちゃと考える](http://d.hatena.ne.jp/bowbow99/20090523/1243043153))

当時の Test::Unit には階層化のメソッドが無かったので、ネストで状況を表現できる `context` は本当に画期的だったんです。(のちに `sub_test_case` として輸入されて、ネストに関しては差が無くなりました。
[test-unit 2.5.2 リリースアナウンス](http://blade.nagaokaut.ac.jp/cgi-bin/scat.rb/ruby/ruby-list/48926))

それなのに今では [rubocop-rspec が検出する](https://github.com/backus/rubocop-rspec/blob/v1.20.1/lib/rubocop/cop/rspec/instance_variable.rb) ぐらい「`let` を使うべき」という書き方が一般化しています。RSpec のリードメンテナである Myron Marston 氏も以下のように述べています。

[ruby on rails - When to use RSpec let()? - Stack Overflow](https://stackoverflow.com/questions/5359558/when-to-use-rspec-let) ([日本語訳](https://qiita.com/jnchito/items/cdd9eef2ed193267c651))

> 私は以下のような理由からインスタンス変数ではなく `let` をいつも使っています。
>
> 1. typoにすぐ気づける
> 2. 無駄な初期化の時間を無くせる
> 3. ローカル変数をそのままletに置き換えられる
> 4. letを使った方が読みやすい

スライド中では「音楽性の違い」と表現しましたが、BDD の語彙だからこそ生み出せた何かが、たかが typo の検出、たかが遅延評価のために無くなってしまってはいませんか。

typo の検出にメリットがあるのは分かりますが、それでも `let` の乱用は書きづらさ、読みづらさを生み出していると、僕は思います。圧倒的に「状況を表現すること」の方に分があります。インスタンス変数でも使って良いんですよ。

```ruby
describe "#male?" do
  subject { @user.male? }
  before { @user = create(:user, gender: gender) }

  context "when male" do
    let(:gender) { User::Gender::MALE }
    it { should eq true }
  end

  context "when female" do
    let(:gender) { User::Gender::FEMALE }
    it { should eq false }
  end
end
```

の `before` が `let(:user)` だったとしたら、「事前条件として user が存在すること」を表現するコードが存在しなくなっちゃうじゃないですか。


ちなみに [rspec-given](https://github.com/rspec-given/rspec-given/) っていう gem があって、
これを使うと rspec を `Given`, `When`, `Then` で書けます。minitest も対応しているし、Natural Assertions は power-assert 以前の時代では最高に書きやすかった。

2013 年ぐらいは生き生きとしていて、かなり方向性が一致しているなーと思いながら手元で使っていた (チームに強要するほどは傾倒できなかった) んだけど、最近は元気ないですねー……。


should 記法という破れた夢
--------------------------------

ここも音楽性の違いが如実に現れているところなんですよね。

RubyKaigi 2017 で右代入の話があったのを覚えていますでしょうか？
左から右に流れるように書きたいから、右に向かって代入したい。

「流れるように書きたい」という理由で言語仕様を考えるぐらいの Rubyist に対して、

```ruby
foo.should == bar
```

と流れるように書いていたコードを

```ruby
expect(foo).to eq bar
```

と書くように強制してきたんですよ。これはもう戦争でしょ！ という怒りの表明です。

我々は `should` 記法をまだ取り上げられたわけではないので、使っても良いんですよ……！
少なくともワンライナーの `should` にはモンキーパッチが使われていないので、言い訳することなく正々堂々と使えるのです。

[AMA: The authors of "Effective Testing with RSpec 3", Myron Marston and Ian Dees : ruby](https://www.reddit.com/r/ruby/comments/73nrhk/ama_the_authors_of_effective_testing_with_rspec_3/) ([日本語訳](https://qiita.com/jnchito/items/3a8d19fd9a30468cafd4)) の

> 私は `subject` やワンライナー構文（例: `it { is_expected...}` ）を使うことはほとんどありません。

についても、「お前に `should` 記法を deprecated にされたから仕方なくワンライナーで書いとるんやんけ」という気持ちが湧いてきますね。

```diff
-foo.stub(bar: buz)
+allow(foo).to receive(:bar).and_return(baz)
```

も流れるように感が減ってますよね。

```ruby
allow().to receive
expect().to receive
expect().to have_received
```

で揃うようになったので綺麗ではあるんだけど。

夢を託していた当時の様子は

* [「RSpec は英語として読みやすいから良い」というお題目はなんだったのか - @kyanny's blog](http://blog.kyanny.me/entry/2012/07/12/234344)
* ["rspec の書き方がわからない" - Togetter](https://togetter.com/li/656392)

辺りを読んでもらうと見えるんじゃないかと思います。

くそー、ホントあの瞬間に Refinements があればなー……。

> ……2012年6月に Refinements があれば RSpec は——
>
> BDD を体現するテストライブラリとしての位置を確立し続けただろう。
>
> でも、そうはならなかった。ならなかったんだよ、ロック。
>
> だから——この話はここでお終いなんだ。

まとめ
--------------------------------

というわけで

* BDD って何？
* Given-When-Then という構造化技法
* `should` 記法という破れた夢

という話をしました。

前 2 つは [新訳版 テスト駆動開発](https://www.amazon.co.jp/dp/B077D2L69C) の『付録 C』に書いてあるのでサラッと流して should 記法の繰り言に持ち込もうと思っていたんですが、思った以上に『付録 C』を読んでいる人が居なかった (会場の 1/3 ぐらいでした) ので説明を丁寧にしていたら時間切れになってしまいました＞＜

[![テスト駆動開発 書影](/images/2017/12/13/test_driven_development_by_example.jpg)](https://www.amazon.co.jp/dp/B077D2L69C)

本当に TDD をとりまくここ 20 年の動きについてよくまとまっている文章なので、一度は読んでみてください。
