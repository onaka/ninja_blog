---
title: TokyuRuby会議09でLTしてきた
date: 2015/08/31T03:14:00 +09:00
tags: rails
ogp:
  og:
    image:
      "": "/images/2015/08/31/slide.gif"
      type: "image/gif"
---

TokyuRuby会議09でLTしてきた #tqrk09
================================

発表資料です。

![当日の発表資料](/images/2015/08/31/slide.gif)

<iframe src="//www.slideshare.net/slideshow/embed_code/key/2zwflfYJz63B1d" width="425" height="355" frameborder="0" marginwidth="0" marginheight="0" scrolling="no" style="border:1px solid #CCC; border-width:1px; margin-bottom:5px; max-width: 100%;" allowfullscreen> </iframe> <div style="margin-bottom:5px"> <strong> <a href="//www.slideshare.net/takafumionaka/application-bootstrap" title="Application Bootstrap" target="_blank">Application Bootstrap</a> </strong> from <strong><a href="//www.slideshare.net/takafumionaka" target="_blank">Takafumi ONAKA</a></strong> </div>

伝えたかったこと
--------------------------------

1コマンドで開発環境が構築できるように**標準化**されてきた歴史。

開発環境構築はみんなプロジェクトに参加した直後にしか行わないからか手抜きされがちで、
僕らも数年前まで

*   OS が入っただけのまっさらなマシンをもらって
*   環境構築手順書を見ながらミドルウェアを入れて
*   ソースコードを clone してきて
*   `git clone` してきたそのままだと動かない各種設定 (`config/database.yml` 等) を行って
*   ダミーデータを突っ込んで
*   ようやくブラウザから見られる (ここまで色んな罠を踏むので半日〜1日かかる)

なんて状況だった。

Rails はこういう細々したイラつきで**減速しないように下支えする**のが得意なコミュニティで、

*   [Setting up a new machine for Ruby development – Signal v. Noise](https://signalvnoise.com/posts/2998-setting-up-a-new-machine-for-ruby-development)

> All our apps has a rake setup task

*   [Ruby Patterns from GitHub's Codebase](http://zachholman.com/talk/ruby-patterns/)

> Most of our Ruby projects have a file called `bootstrap` in the `script/` directory.

と「叩けばセットアップが完了する」「共通のコマンド」がプロジェクト横断的に用意されるのが普通だったし、
`rails new` するときのテンプレートに含まれるようになった。

この思想に従って、`bin/setup` をちゃんと使うことで**知らないプロジェクトにいつ入っても5分後からコードを書ける**状態を作れるので是非このレールに乗ってください。

仮想化全盛期でもまだ有効なレールです。


当日の様子
--------------------------------

冒頭のアニメーション gif のような異常に見づらいプレゼンとなっておりました。

ついったーでも大人気！

<blockquote class="twitter-tweet" lang="en"><p lang="ja" dir="ltr">読みづらいｗ <a href="https://twitter.com/hashtag/tqrk09?src=hash">#tqrk09</a></p>&mdash; tsuka (@tsuka) <a href="https://twitter.com/tsuka/status/637538349738168320">August 29, 2015</a></blockquote>

<blockquote class="twitter-tweet" lang="en"><p lang="ja" dir="ltr">ひたすらに読みづらいｗ　<a href="https://twitter.com/hashtag/tqrk09?src=hash">#tqrk09</a></p>&mdash; Keisuke.Izumiya (@syguer) <a href="https://twitter.com/syguer/status/637538403379081216">August 29, 2015</a></blockquote>

<blockquote class="twitter-tweet" lang="en"><p lang="ja" dir="ltr">資料やばすぎるｗ <a href="https://twitter.com/hashtag/tqrk09?src=hash">#tqrk09</a></p>&mdash; ᴋᴏᴋᴜʙᴜɴ (@k0kubun) <a href="https://twitter.com/k0kubun/status/637538649681235968">August 29, 2015</a></blockquote>

<blockquote class="twitter-tweet" lang="en"><p lang="ja" dir="ltr">3Dプレゼン読みづらすぎるｗ <a href="https://twitter.com/hashtag/tqrk09?src=hash">#tqrk09</a></p>&mdash; カイバ (@kaiba) <a href="https://twitter.com/kaiba/status/637538738805932032">August 29, 2015</a></blockquote>

<blockquote class="twitter-tweet" lang="en"><p lang="ja" dir="ltr">読み辛すぎてウケるｗ <a href="https://twitter.com/hashtag/tqrk09?src=hash">#tqrk09</a></p>&mdash; Jewelve (@jewel_x12) <a href="https://twitter.com/jewel_x12/status/637538806623662080">August 29, 2015</a></blockquote>

<blockquote class="twitter-tweet" lang="en"><p lang="ja" dir="ltr">読めないプレゼン資料 <a href="https://twitter.com/hashtag/tqrk09?src=hash">#tqrk09</a></p>&mdash; Yasuyuki Inoue (@YaSuYuKi) <a href="https://twitter.com/YaSuYuKi/status/637538939113373696">August 29, 2015</a></blockquote>


資料の説明
--------------------------------

[processing](https://processing.org/) で作りました。

スケジュールが発表されて、内容で負けているのが見えたので LT 王に向けて飛び道具を用意したかったんだ。
読めないのはダメですね。本当にすみませんでした。

<blockquote class="twitter-tweet" lang="en"><p lang="ja" dir="ltr">資料の構成だいぶ固まったのでプレゼンツールを作らなければならない</p>&mdash; Takafumi ONAKA (@onk) <a href="https://twitter.com/onk/status/636094058037796864">August 25, 2015</a></blockquote>
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

構想2日、実装1日です。初めて 3D で描画したｗ

[processing - 3D空間に発光するパーティクルを配置する - Qiita](http://qiita.com/clomie/items/7134a4b627426a8269b4)
が超カッコ良かったので、このキラキラを背景にして資料の立方体が浮いてるとソレっぽいんじゃないかと思った。簡単に見栄えが良いものが作れるのでもっと流行ると良いなぁと思う。

