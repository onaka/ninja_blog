---
title: RailsDevCon 2010 で話してきた
date: 2010-11-22T05:56:12 +09:00
tags: rails,event
ogp:
  og:
    image: "/images/2010/11/22/page_1-6.png"
---

RailsDevCon 2010 で話してきた
================================

こんにちは！ onk です。

一昨日の 11/20 に [RailsDevCon2010](http://railsdevcon.jp/) にスピーカーとして参加してきました。場所はオラクル青山センターさん。

聞いていただいた皆さま，ありがとうございました。
いやー，30 分も話すの初めてだったので緊張したｗ 実行委員の皆さまもお疲れさまでした！

プレゼン資料はこちら⇒[とあるアプリの開発運用(トラブルシュート)](http://www.slideshare.net/takafumionaka/ss-5852561)

<iframe src="//www.slideshare.net/slideshow/embed_code/key/deFEgdUWwdaxur" width="425" height="355" frameborder="0" marginwidth="0" marginheight="0" scrolling="no" style="border:1px solid #CCC; border-width:1px; margin-bottom:5px; max-width: 100%;" allowfullscreen> </iframe> <div style="margin-bottom:5px"> <strong> <a href="//www.slideshare.net/takafumionaka/ss-5852561" title="とあるアプリの開発運用(トラブルシュート)" target="_blank">とあるアプリの開発運用(トラブルシュート)</a> </strong> from <strong><a href="//www.slideshare.net/takafumionaka" target="_blank">Takafumi ONAKA</a></strong> </div>

内容ですが，

> ソーシャルならではの負荷分散、インフラ、ログ解析とかが聞けると夢が広がっていいかも。他セッションと絶対かぶらないし。大規模サービス作ろうとしている人にはありがたいと思います。

と [@2celeb](http://twitter.com/2celeb) さんからリクエストがあったので，『聞くだけで運用した気になるソーシャルアプリ』を主題にしました。駆け足でしたが，伝えたいことは盛り込めたかなぁと思います。少しでも参考になれば幸いです。

関連記事
--------------------------------

* [RailsDevCon2010](http://railsdevcon.jp/)
* [Togetter - 「RailsDevCon2010のハッシュタグ(#railsdevcon)のまとめ」](http://togetter.com/li/70973)
* [RailsDevCon2010に行ってきた - 平凡なエンジニアの独り言](http://d.hatena.ne.jp/akasata/20101120/1290269573)
* [Rails Developer Conference2010 聴講記 - TrinityT’s LABO](http://d.hatena.ne.jp/TrinityT/20101120/1290264975)
* [RailsDevCon2010に参加してきました - exdesign](http://blog.exdesign.jp/railsdevcon2010)
* [Rails Dev Con に行ってきた :: marugoshi.org](http://www.marugoshi.org/roma-was-not-built-in-a-day/2010/11/20/rails-dev-con/)
* [UKSTUDIO - RailsDevCon2010で話してきました](http://ukstudio.jp/2010/11/22/railsdevcon2010/)
* [RailsDevCon 2010にてCucumberの始め方の話をしました - moroの日記](http://d.hatena.ne.jp/moro/20101120/1290243692)


以下，twitter での反応と補足
--------------------------------

### Page 1-6

![page 1-6](/images/2010/11/22/page_1-6.png)

> * [sasata299](http://twitter.com/sasata299)タイトル画面ww #RailsDevCon
> * [ikm](http://twitter.com/ikm)一瞬見えたロゴがw #railsdevcon
> * [sgtakeru](http://twitter.com/sgtakeru)@onk さんのセッション開始 #RailsDevCon
> * [d6rkaiz](http://twitter.com/d6rkaiz)#RailsDevCon とあるソーシャルアプリの開発運用 ＠onk さん
> * [DiscoveryCoach](http://twitter.com/DiscoveryCoach)３つ目のセッション、とあるアプリの運用開発。 (#railsdevcon live at http://ustre.am/qnNA)
> * [ClockWorkStudio](http://twitter.com/ClockWorkStudio)とあるソーシャルゲームの開発運用(トラブルシュート) #RailsDevCon
> * [ikm](http://twitter.com/ikm)意外と会場にソーシャルゲームを作った人が少ない #railsdevcon
> * [oukayuka](http://twitter.com/oukayuka)O・N・K! O・N・K!! #railsdevcon

話し始める前にソーシャルゲームを開発したことがある人に挙手してもらったんですがほとんど居ませんでした。遊んでる人で半分ぐらいだったかな。

ロゴはおなじみ [http://to-a.ru/](http://to-a.ru/) ですね。名付け親は [@youthk](http://twitter.com/youthk)。

### page7-12

![page 7-12](/images/2010/11/22/page_7-12.png)

> * [sgtakeru](http://twitter.com/sgtakeru)ソーシャルアプリは、API利用、人口爆発、短納期 #RailsDevCon
> * [DiscoveryCoach](http://twitter.com/DiscoveryCoach)まずはソーシャルアプリのＡＰＩ利用の話 (#railsdevcon live at http://ustre.am/qnNA)
> * [DiscoveryCoach](http://twitter.com/DiscoveryCoach)開発はＡＰＩを使った場合のデバックの仕方が肝。 (#railsdevcon live at http://ustre.am/qnNA)
> * [ar1](http://twitter.com/ar1)ハンゲームでけー #RailsDevCon つか、800万人いるのかドリコムのソーシャルゲー．

あ，p11 も p12 もアクティブユーザ数じゃなくて会員数です。念のため。適当にプレスリリースから拾ってきたので半年分ぐらい誤差があると思う。

### page 13-18

![page 13-18](/images/2010/11/22/page_13-18.png)

> * [cesare](http://twitter.com/cesare)#RailsDevCon 「大安なのでリリース」ｗ
> * [ikm](http://twitter.com/ikm)大安大事 #railsdevcon
> * [agilekawabata](http://twitter.com/agilekawabata)大安でリリース #RailsDevCon
> * [ar1](http://twitter.com/ar1)大安なのでリリース(w #RailsDevCon
> * [oukayuka](http://twitter.com/oukayuka)ソーシャルアプリは2ヶ月でリリースがデフォ #railsdevcon
> * [sgtakeru](http://twitter.com/sgtakeru)大安でリリース＆バージョンアップ #RailsDevCon
> * [tyabe](http://twitter.com/tyabe)#RailsDevCon リリース日は大安
> * [adzuki34](http://twitter.com/adzuki34)「大安でリリース」 #RailsDevCon
> * [hs9587](http://twitter.com/hs9587)#RailsDevCon 「とあるソーシャルアプリの開発運用」 大安なのでリリース
> * [DiscoveryCoach](http://twitter.com/DiscoveryCoach)２ヶ月でリリースは想定内。短納期をいかにこなすか。何を犠牲にして何を得るのか。。。が大事。 (#railsdevcon live at http://ustre.am/qnNA)
> * [agilekawabata](http://twitter.com/agilekawabata)大安リリース駆動開発だな #RailsDevCon
> * [ClockWorkStudio](http://twitter.com/ClockWorkStudio)聴いたこともないgemがズラッと並んでた #RailsDevCon
> * [ikm](http://twitter.com/ikm)unicorn+nginx+rails3+ruby1.9.2など、今風の環境 #railsdevcon
> * [oreradio](http://twitter.com/oreradio)ドリコムさんもRedis使ってるのね。 #RailsDevCon
> * [TrinityT](http://twitter.com/TrinityT)ミドルウェア、ことごとく最新だ。unicorn2, nginx0.7, Rails3, MySQL5.1, Ruby1.9.2, Redis.... やるなドリコム！ #RailsDevCon
> * [Hexa](http://twitter.com/Hexa)Redis 使っているのか。 #RailsDevCon
> * [d6rkaiz](http://twitter.com/d6rkaiz)#RailsDevCon ドリコムさんでの環境一例 nginx+unicorn+mysql+memcached+redis+rails

「2ヶ月でリリースがデフォ」はさすがに言い過ぎっす……。

ミドルウェアは大体最新に合わせるようにしてますねー。「RailsConf 2010 に合わせてリリースされるだろう」と Rails3 beta で開発した挙句 rc4 の状態で本番投入したりとか，Passenger3 で地雷踏んだりとかしています(

### page 19-24

![page 19-24](/images/2010/11/22/page_19-24.png)

> * [ar1](http://twitter.com/ar1)ドリコムのミドルウェア環境のversionはかなりあたらしいので揃えてるな。L5520 x2で32GBメモリの上に仮想化。8つくらいのVM #RailsDevCon
> * [ar1](http://twitter.com/ar1)#RailsDevCon リクエスト&amp;レスポンスを確認しろ! HTTPのステータスコードをみろ!
> * [cesare](http://twitter.com/cesare)#RailsDevCon 「困ったら TCP まで降りる」基本ですなｗ
> * [ikm](http://twitter.com/ikm)困ったらリクエストとレスポンスを見る。TCPレイヤーで見るのが確実 #railsdevcon
> * [DiscoveryCoach](http://twitter.com/DiscoveryCoach)バグで3困ったらＴＣＰレイヤまで降りてパケットを確認しよう。 (#railsdevcon live at http://ustre.am/qnNA)
> * [sgtakeru](http://twitter.com/sgtakeru)デバックは、困ったら、request/resonseまでみる。TCPレイヤーまで降りてパケットを見る。 #RailsDevCon
> * [arihh](http://twitter.com/arihh)パケット監視マジ重要 #RailsDevCon
> * [takai](http://twitter.com/takai)TCPじゃなくてHTTPだよね

はい，HTTP でしたごめんなさい！

アプリの中から見ているだけだと oauth や jpmobile の挙動によく泣かされたので，API とやり取りするときは必ず WAF が処理する前の request を眺めるようにしています。

サーバ構成はあくまで基本構成で，本番は web がズラーっと横に並んだり cache や db の slave が並んだりします。

### page 25-30

![page 25-30](/images/2010/11/22/page_25-30.png)

> * [ar1](http://twitter.com/ar1)#RailsDevCon Net::HTTPにloggerをしこむ net-http-spy. というかこれがrubyねた最初?
> * [ClockWorkStudio](http://twitter.com/ClockWorkStudio)net::httpにloggerをしこむnet-http-spy #RailsDevCon
> * [TokyoIncidents](http://twitter.com/TokyoIncidents)net-http-spy #RailsDevCon
> * [DiscoveryCoach](http://twitter.com/DiscoveryCoach)かなり具体的ですごい。会場の文字入力のかちゃかちゃという音が今日最大かも。。ｗｗ (#railsdevcon live at http://ustre.am/qnNA)
> * [ikm](http://twitter.com/ikm)net-http-spyでNet::HTTPにロガーを仕込む。超便利そう #railsdevcon
> * [TrinityT](http://twitter.com/TrinityT)net-http-spy tcpflow #RailsDevCon
> * [hs9587](http://twitter.com/hs9587)#RailsDevCon 「とあるソーシャルアプリの開発運用」 問題発見は request と response を見る、眺めてれば分かる事も多い。 net-http-spy Net::HTTP にログを仕込む
> * [ar1](http://twitter.com/ar1)#RailsDevCon tcpflowつかってんのか。。yumとかaptをみたり。gree行きのも見ると。。
> * [d6rkaiz](http://twitter.com/d6rkaiz)#RailsDevCon tcpflow // tcpdumpの保存しないバージョン
> * [masaki925](http://twitter.com/masaki925)net-http-spy でTCPパケットロギングしてデバッグ #RailsDevCon
> * [TokyoIncidents](http://twitter.com/TokyoIncidents)tcpflow #RailsDevCon

[martinbtt's net-http-spy at master - GitHub](https://github.com/martinbtt/net-http-spy) ですね。外にアクセスしてるのが一目瞭然で便利なので，僕は .irbrc に

```ruby
require "net-http-spy"
```

を書いています。

tcpflow は yum や apt で入れてください。ファイルに落としたいときってそんなに無いので flow で十分なんですよねー。

### page 31-36

![page 31-36](/images/2010/11/22/page_31-36.png)

> * [_shimada](http://twitter.com/_shimada)タイトルに反して超実践的で参考になる (#railsdevcon live at http://bit.ly/a480k3
> * [nog](http://twitter.com/nog)凄く具体的な話してるなー。 (#railsdevcon live at http://ustre.am/qnNA)
> * [DiscoveryCoach](http://twitter.com/DiscoveryCoach)巨大ＳＮＳのアプリだとスモールスタートできない。。。 (#railsdevcon live at http://ustre.am/qnNA)
> * [nog](http://twitter.com/nog)Rails1インスタンス200MB?多くね？ (#railsdevcon live at http://ustre.am/qnNA)
> * [ar1](http://twitter.com/ar1)#RailsDevCon スモールスタートさせてくれない。1ヶ月で30万突破を視野にしておく。10万DAUで 1000万imp/day -&gt; 230imp/s 。Railsは200MB workerだと かけ算で40G.
> * [ar1](http://twitter.com/ar1)#RailsDevCon m1.xlargeの限界は2300QPSくらい
> * [ClockWorkStudio](http://twitter.com/ClockWorkStudio)230imp/secだった場合の必要なマシンリソースの説明が具体的すぎてすげえ #RailsDevCon
> * [tkawa](http://twitter.com/tkawa)スモールスタート機能(?)あるといいかも。招待制にするとかすればいいのかな？ (#railsdevcon live at http://ustre.am/qnNA)
> * [DiscoveryCoach](http://twitter.com/DiscoveryCoach)マシンのスペックまでも。。。もう明日からはじめるソーシャルアプリ的な話ですね。 (#railsdevcon live at http://ustre.am/qnNA)

CPU 使用率はアプリによって違うだろうから，分かりやすくメモリ使用量で表現しています。150MB 超えることもあるので念のため 200 弱と言っておきました！

### page 37-42

![page 37-42](/images/2010/11/22/page_37-42.png)

> * [ikm](http://twitter.com/ikm)データ量の増加が著しいのでmaster/slave必須 #railsdevcon
> * [sasata299](http://twitter.com/sasata299)ソーシャルゲームの規模感は凄いなぁー #RailsDevCon
> * [ysakaki](http://twitter.com/ysakaki)@onk の発表実践的で凄いな。こういう話がもっと聞きたい！ #RailsDevCon
> * [TokyoIncidents](http://twitter.com/TokyoIncidents)このスライドは絶対復習したい #RailsDevCon
> * [ar1](http://twitter.com/ar1)#RailsDevCon マイミク村. ともだちコレクションみたいなもの。質問を全て保存するのでしんどい。DBをなんとかする。
> * [shinodogg](http://twitter.com/shinodogg)ソーシャルゲームのキャパシティプランニングの話。コレは参考になるなぁ。こういうのを大安の日ドリブンみたいなノリで継続してリリースし続けるのはカッコイイ。 #railsdevcon
> * [DiscoveryCoach](http://twitter.com/DiscoveryCoach)ＤＢのmaster/slave分散を考える。 (#railsdevcon live at http://ustre.am/qnNA)
> * [johnsmith0707](http://twitter.com/johnsmith0707)スケールアウトか。puppetとかもあるように、最近はほんとにスケーラビリティの高さが求められるよね。 #RailsDevCon
> * [ikm](http://twitter.com/ikm)master/slaveはMasochism #railsdevcon
> * [ar1](http://twitter.com/ar1)#RailsDevCon master/slave分散。参照系だったらslaveを見にいく、updateはmasterに、とかはアプリで書く。ライブラリつかってない
> * [_shimada](http://twitter.com/_shimada)ActiveRecordでmaster/slave分散は難しくない (#railsdevcon live at http://bit.ly/a480k3

* [Revolution On Rails: [PLUGIN RELEASE] ActsAsReadonlyable](http://revolutiononrails.blogspot.com/2007/04/plugin-release-actsasreadonlyable.html)
* [schoefmax's multi_db at master - GitHub](https://github.com/schoefmax/multi_db)
* [technoweenie's masochism at master - GitHub](https://github.com/technoweenie/masochism)
* [kovyrin's db-charmer at master - GitHub](https://github.com/kovyrin/db-charmer)
* [fiveruns's data_fabric at master - GitHub](https://github.com/fiveruns/data_fabric)
* [tchandy's octopus at master - GitHub](https://github.com/tchandy/octopus)

DB 切り替えてる部分はどれも 100 行無いぐらいなので読んでみるといいです。使い方じゃなくて中身に言及している記事が増えると世界が面白くなる。

### page 43-48

![page 43-48](/images/2010/11/22/page_43-48.png)

 > * [ar1](http://twitter.com/ar1)#RailsDevCon JOINできないから、RDB関連は消す。habtmとか。
 > * [junya](http://twitter.com/junya)ゲームはEventually Consistentだとまずいデータがほとんどだから面倒くさそうだ。 #RailsDevCon
 > * [DiscoveryCoach](http://twitter.com/DiscoveryCoach)更新系をmasterに、参照系をslaveにまわしてみたが、ゲームのアプリは更新系が多くて限界がくる。例：体力減る。 (#railsdevcon live at http://ustre.am/qnNA)
 > * [TrinityT](http://twitter.com/TrinityT)マスタDBをテーブルごとに分割→has_manyやbelogs_toが使えなくなる→関連はAP側で実装か〜。 凄い試行錯誤だったんだろうな。 #RailsDevCon

すみません見栄張りました。分割はカテゴリごとなので，そのカテゴリ内では JOIN 可能です。なので自分でクエリ投げ直す処理はそんなに発生しないですね。

### page 49-54

![page 49-54](/images/2010/11/22/page_49-54.png)

> * [ar1](http://twitter.com/ar1)#RailsDevCon rails3のshardingにはoctopus. 最近はいいハードなのでscaleupで対応
> * [ikm](http://twitter.com/ikm)shardingはdb-charmer, data_fabric, octopus #railsdevcon
> * [hs9587](http://twitter.com/hs9587)#RailsDevCon 「とあるソーシャルアプリの開発運用」 　Sharding 、DB のレプリケーションと分割、そういう gem もある
> * [ar1](http://twitter.com/ar1)#RailsDevCon MongoDBにするのは悩み中。AR捨てるのが..
> * [sasata299](http://twitter.com/sasata299)master-slaveはmasochizm、shardingはoctopusを使ってやってるらしい #RailsDevCon
> * [_shimada](http://twitter.com/_shimada)テストデータはfakerで作成 (#railsdevcon live at http://bit.ly/a480k3
> * [agilekawabata](http://twitter.com/agilekawabata)テストデータ生成に便利なgem faker #RailsDevCon
> * [oukayuka](http://twitter.com/oukayuka)負荷テスト用のダミーデータを作ってくれるgem &gt; faker #railsdevcon
> * [ClockWorkStudio](http://twitter.com/ClockWorkStudio)50万ユーザが3ヶ月遊んだ想定のデータを作成した上でのアクセス負荷試験 #RailsDevCon
> * [DiscoveryCoach](http://twitter.com/DiscoveryCoach)負荷テストは５０万ユーザが３ヶ月遊んだ想定のデータを作成してｍ本番想定と同じアクセス負荷をかけて行う。 (#railsdevcon live at http://ustre.am/qnNA)
> * [RailsDevCon](http://twitter.com/RailsDevCon)負荷テストのためにリリース前に50万ユーザが3ヶ月遊んだ想定のデータを突っ込む。 #RailsDevCon
> * [sasata299](http://twitter.com/sasata299)"MongoDBを使えば解決するんじゃないか”ｗ #RailsDevCon
> * [ar1](http://twitter.com/ar1)#RailsDevCon jmeter + fakerでデータ生成 で、負荷テスト．スループット，DiskIO、コネクション数に注目(webとdb)。

スケールアップで対応できちゃったので，octopus まだ使ってません。でも今使うならコレかなぁと考えています。

MongoDB は試してみたいんですが，Mongo 脳になって綺麗な設計が出来るようになるまでにまた 3 アプリぐらいかかるんだろうな。

負荷テストと監視については YAPC::Asia 2010 での myfinder さんの発表が良かったです。[myfinder's blog: YAPC::Asia 2010 でソーシャルアプリのシステム監視運用について Talk してきました](http://blog.myfinder.jp/2010/10/yapcasia-2010-talk.html)

### page 55-60

![page 55-60](/images/2010/11/22/page_55-60.png)

> * [DiscoveryCoach](http://twitter.com/DiscoveryCoach)基本、最速でユーザにレスポンスを返す。後は裏で非同期。 (#railsdevcon live at http://ustre.am/qnNA)
> * [ikm](http://twitter.com/ikm)TOPへのアクセスをトリガーにして各種ページの準備を事前に実行する、とか #railsdevcon

* [defunkt's resque at master - GitHub](https://github.com/defunkt/resque)
* [collectiveidea's delayed_job at master - GitHub](http://github.com/collectiveidea/delayed_job)

### page 61-66

![page 61-66](/images/2010/11/22/page_61-66.png)

> * [nog](http://twitter.com/nog)メモ：Resque (#railsdevcon live at http://ustre.am/qnNA)
> * [_shimada](http://twitter.com/_shimada)非同期処理は Resque。 管理画面あり (#railsdevcon live at http://bit.ly/a480k3
> * [sasata299](http://twitter.com/sasata299)Resqueで非同期処理をガンガン使ってるみたい。マイページ開いたときにはデータが出来上がってる想定みたいだけど間に合わなかったときはどうするんだろう #RailsDevCon
> * [ar1](http://twitter.com/ar1)#RailsDevCon 5秒ルールのせいで、非同期を徹底活用。Resque gemをつかっている。管理画面がいい。バッチ処理も簡単。
> * [ClockWorkStudio](http://twitter.com/ClockWorkStudio)非同期実行のgem、rescue。管理画面が便利らしい #RailsDevCon
> * [d6rkaiz](http://twitter.com/d6rkaiz)#RailsDevCon 非同期で扱うためのgemにresque
> * [ar1](http://twitter.com/ar1)#RailsDevCon まとめた処理にactiverecord-importを使っている。
> * [agilekawabata](http://twitter.com/agilekawabata)一括insert gem activerecord-import #RailsDevCon
> * [sasata299](http://twitter.com/sasata299)activerecord-import使うとバルクインサートが楽？へー #RailsDevCon
> * [ClockWorkStudio](http://twitter.com/ClockWorkStudio)activerecord-importでBULK INSERT!! #RailsDevCon
> * [_shimada](http://twitter.com/_shimada)Resque Scheduler。毎回script/runnerを叩かなくていい (#railsdevcon live at http://bit.ly/a480k3
> * [DiscoveryCoach](http://twitter.com/DiscoveryCoach)ゆーざーが100万人くらいになると「手抜きかな」と思いながら創った部分が必ず「ボトルネック (#railsdevcon live at http://ustre.am/qnNA)
> * [ikm](http://twitter.com/ikm)シビアだ #railsdevcon
> * [oreradio](http://twitter.com/oreradio)ユーザ数数万程度を想定しているコードは全てNG #RailsDevCon
> * [n0ts](http://twitter.com/n0ts)https://github.com/zdennis/activerecord-import RT @sasata299: activerecord-import使うとバルクインサートが楽？へー #RailsDevCon
> * [TrinityT](http://twitter.com/TrinityT)「ユーザ数、数万で想定しているコードは全てNG」→バッチで1ユーザあたり1秒かかってたら、一日86400ユーザ分しか処理できない！  #RailsDevCon
> * [d6rkaiz](http://twitter.com/d6rkaiz)#RailsDevCon ユーザ数数万程度のコードは全てNG

* [zdennis's activerecord-import at master - GitHub](https://github.com/zdennis/activerecord-import)

そういえば Rails3 から route が Rack に載ったので，sinatra アプリである Resque の管理画面も一緒に動かせるようになりましたね。

### page 67-72

![page 67-72](/images/2010/11/22/page_67-72.png)

> * [ar1](http://twitter.com/ar1)#RailsDevCon GAEの設計の意味がわかってくる(w
> * [ikm](http://twitter.com/ikm)巨大SNSのユーザ数で自分の技術力が向上 #railsdevcon
> * [DiscoveryCoach](http://twitter.com/DiscoveryCoach)巨大ＳＮＳから流れてくるユーザ数はエンジニアの甘えを許さない。 (#railsdevcon live at http://ustre.am/qnNA)
> * [junya](http://twitter.com/junya)URL設計図って rake routes で間に合う気もする #RailsDevCon
> * [_shimada](http://twitter.com/_shimada)開発中はER図とURL設計書だけで進める (#railsdevcon live at http://bit.ly/a480k3
> * [ysakaki](http://twitter.com/ysakaki)迷ったらRailsっぽく書く。素晴らしい。 #RailsDevCon
> * [ar1](http://twitter.com/ar1)#RailsDevCon PDCAを早くするためドキュメントを削減。図の多用。Railsっぽいコードから外れないようなコード。
> * [hs9587](http://twitter.com/hs9587)#RailsDevCon 「とあるソーシャルアプリの開発運用」 迷う時間を最小限にする: Railsぽいかどうかを考える
> * [ikm](http://twitter.com/ikm)RESTfulっていう前提はあるかなぁ RT @junya: URL設計図って rake routes で間に合う気もする #RailsDevCon
> * [nog](http://twitter.com/nog)ソーシャルゲームもこういう観点で見ると面白そうだよなー (#railsdevcon live at http://ustre.am/qnNA)
> * [tkawa](http://twitter.com/tkawa)「正しくwebアプリを作る」それを「正しく」って言い切るのもなぁｗ (#railsdevcon live at http://ustre.am/qnNA)
> * [johnsmith0707](http://twitter.com/johnsmith0707)ペアプロで迷う時間を最小限に #RailsDevCon
> * [DiscoveryCoach](http://twitter.com/DiscoveryCoach)短納期に対応するひとつの方法。迷う時間を少なくする。＝ペアプロ。そしてRailsっぽいかどうかを思考の指針できるように常に考える。 (#railsdevcon live at http://ustre.am/qnNA)
> * [shinodogg](http://twitter.com/shinodogg)ドキュメンテーションなくても、RailsっぽいアプリならER図見りゃわかるし、RestっぽいインタフェースならURL見れば分かる。迷ったらRailsっぽくなってるかどうか。くぅー。シビれる。。 #railsdevcon

負荷対策をすればするほど GAE に近づいていっている気がしたので，なるほどあの制限は正しいなと思えるようになりました。
一度しっかり RDBMS で戦うと NoSQL や GAE を使うための意識改革が出来るんじゃないかと思います。

URL 設計書は確かに rake routes と中身は同じなんですが，チーム全員で URL を考えるときに 200 行を超える routes.rb だけだと見通しが悪いので，はじめは Excel でやっています。
設計が終わって一通り generate し終えたら routes.rb だけ管理で十分ですねー。

### page 73-78

![page 73-78](/images/2010/11/22/page_73-78.png)

> * [ar1](http://twitter.com/ar1)#RailsDevCon Plan (常にかんがえる) Check (かってにデータがあつまってくる) 速度をあげる。

ちなみに，天井からぶら下がってるこのモニタのシステムは『グリゴリ』と言います。
旧約聖書偽典『エノク書』 (みんな大好きイーノックさん) に出る堕天使の一団。「見張る者」という意味です。

[「ユーザーの動きに応じて、ＰＤＣＡサイクルを異様なほどの超高速で回すのが、ソーシャルゲームの運営だ」](http://www.enpitu.ne.jp/usr6/bin/day?id=60769&amp;pg=20101023) といった話も紹介しようと思ったんですが残り時間が少なかったのでカット。

### page 79-83

![page 79-83](/images/2010/11/22/page_79-83.png)

> * [ar1](http://twitter.com/ar1)#RailsDevCon ログの集約、収集にscribeを使ってる!
> * [ikm](http://twitter.com/ikm)scribe+messagepack #railsdevcon
> * [ar1](http://twitter.com/ar1)#RailsDevCon scribeにつっこむときにmessage-packで圧縮考えてる。
> * [hs9587](http://twitter.com/hs9587)#RailsDevCon 「とあるソーシャルアプリの開発運用」 scribe 分散した多数のサーバのログの集約が大変
> * [d6rkaiz](http://twitter.com/d6rkaiz)#RailsDevCon 一日3gbのlogを見るために scribe -&gt; messagepack にするか検討中？
> * [sasata299](http://twitter.com/sasata299)複数台に分かれたログの収集はscribe使ってる http://d.hatena.ne.jp/perezvon/20100110/1263120529 #RailsDevCon
> * [masaki925](http://twitter.com/masaki925)ログ転送ライブラリ scribe; scribeサーバに送っておけばscribe 間で転送・エラーハンドリングしてくれる #RailsDevCon


## いじょー

長々と失礼しました。また来年も発表できるよう頑張ります！！

