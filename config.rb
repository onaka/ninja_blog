###
# Blog settings
###

# Time.zone = "UTC"
Time.zone = "Tokyo"

activate :blog do |blog|
  # This will add a prefix to all links, template references and source paths
  # blog.prefix = "blog"

  blog.permalink = "{year}/{month}/{day}/{title}"
  # Matcher for blog source files
  # blog.sources = "{year}-{month}-{day}-{title}.html"
  # blog.taglink = "tags/{tag}.html"
  # blog.layout = "layout"
  # blog.summary_separator = /(READMORE)/
  # blog.summary_length = 250
  # blog.year_link = "{year}.html"
  # blog.month_link = "{year}/{month}.html"
  # blog.day_link = "{year}/{month}/{day}.html"
  blog.default_extension = ".md"

  blog.tag_template = "tag.html"
  blog.calendar_template = "calendar.html"

  # Enable pagination
  # blog.paginate = true
  # blog.per_page = 10
  # blog.page_link = "page/{num}"
  blog.publish_future_dated = true
end

page "/feed.xml", layout: false

###
# Compass
###

# Change Compass configuration
# compass_config do |config|
#   config.output_style = :compact
# end

###
# Page options, layouts, aliases and proxies
###

# Per-page layout changes:
#
# With no layout
# page "/path/to/file.html", layout: false
#
# With alternative layout
# page "/path/to/file.html", layout: :otherlayout
#
# A path which all have the same layout
# with_layout :admin do
#   page "/admin/*"
# end

# Proxy pages (https://middlemanapp.com/advanced/dynamic_pages/)
# proxy "/this-page-has-no-template.html", "/template-file.html", locals: {
#  which_fake_page: "Rendering a fake page with a local variable" }

###
# Helpers
###

# Automatic image dimensions on image_tag helper
# activate :automatic_image_sizes

# Methods defined in the helpers block are available in templates
# helpers do
#   def some_helper
#     "Helping"
#   end
# end
helpers do
  def render_toc(page)
    toc = Greenmat::Markdown.new(
      Qiita::Markdown::Greenmat::HTMLToCRenderer
    )
    text = ::Middleman::FileRenderer.new(@app, page.file_descriptor[:full_path].to_s).template_data_for_file
    toc.render(preprocessing_toc(text))
  end

  def preprocessing_toc(text)
    # シンタックスハイライト内などに#があるとそれもtocとして出てくるので先に消す
    # 足りなかったら都度対応
    text = text.gsub(/```.*?```/m, "") # バッククオート内部を消す
    text = text.gsub(/~~~.*?~~~/m, "") # シンタックスハイライト内を消す
    text = text.gsub(/#+(?![ #])/m, "")  # 空白のない#は見出しではない
    text = text.gsub(/<("[^"]*"|'[^']*'|[^'">])*>/, "") # HTMLタグは除去する
    text = text.gsub(/^>.*/, "") # 引用行を消す
    text = text.gsub(/^(#+)(.*?):(.*?):/) { "#{Regexp.last_match(1)}#{Regexp.last_match(2)}#{Regexp.last_match(3)}" } # 目次に絵文字があったらコロンを取り除く
    text
  end
end

set :css_dir, 'stylesheets'

set :js_dir, 'javascripts'

set :images_dir, 'images'

# Build-specific configuration
configure :build do
  # For example, change the Compass output style for deployment
  # activate :minify_css

  # Minify Javascript on build
  # activate :minify_javascript

  # Enable cache buster
  # activate :asset_hash

  # Use relative URLs
  # activate :relative_assets

  # Or use a different image path
  # set :http_prefix, "/Content/images/"
end

# Markdown configuration
module Middleman
  module Renderers
    class GreenmatTemplate < Tilt::Template
      def prepare
        @engine ||= ::Greenmat::Markdown.new(
          Qiita::Markdown::Greenmat::HTMLRenderer.new(with_toc_data: true),
          autolink: true,
          fenced_code_blocks: true,
          footnotes: true,
          no_intra_emphasis: true,
          no_mention_emphasis: true,
          strikethrough: true,
          tables: true,
        )
      end

      def evaluate(scope, locals, &block)
        @output ||= begin
          content = @engine.render(data)
          processor = Qiita::Markdown::Processor.new
          def processor.filters
            [
              Qiita::Markdown::Filters::Greenmat,
              Qiita::Markdown::Filters::ImageLink,
              Qiita::Markdown::Filters::Footnote,
              Qiita::Markdown::Filters::CodeBlock,
              Qiita::Markdown::Filters::Checkbox,
              HTML::Pipeline::EmojiFilter,
              # Qiita::Markdown::Filters::SyntaxHighlight,
              Qiita::Markdown::Filters::Mention,
              # Qiita::Markdown::Filters::Sanitize,
            ]
          end
          result = processor.call(content)
          result[:output].to_s.html_safe
        end
      end
    end
  end
end

# Keep slug undescore
module ::Middleman
  module Blog
    module BlogArticle
      def slug
        if data['slug']
          Blog::UriTemplates.safe_parameterize(data['slug'])
        elsif blog_data.source_template.variables.include?('title')
          # FIX THIS
          # Blog::UriTemplates.safe_parameterize(path_part('title'))
          path_part('title')
        elsif title
          Blog::UriTemplates.safe_parameterize(title)
        else
          raise "Can't generate a slug for #{path} because it has no :title in its path pattern or title/slug in its frontmatter."
        end
      end
    end
  end
end

set :markdown_engine_prefix, Middleman::Renderers
set :markdown_engine, :greenmat

set :url_root, 'https://blog.onk.ninja'
