namespace :nginx do
  include Capistrano::Util

  desc "Put configuration files for specific environment. (see config/deploy/templates/nginx)"
  task :setup do
    on roles(:web), in: :sequence do
      sudo "mkdir -p /var/log/nginx/#{application}/"
      sudo "chown -R www-data /var/log/nginx/#{application}/"
      sudo "chgrp -R www-data /var/log/nginx/#{application}/"

      root_backup_file("/etc/nginx/sites-available/#{application}")
      root_upload_file(File.join(templates_path, "nginx", "format.ltsv.conf"), "/etc/nginx/conf.d/format.ltsv.conf")
      root_upload_file(File.join(templates_path, "nginx", "sites-available.conf.erb"), "/etc/nginx/sites-available/#{application}")
      if fetch(:use_basic_auth, false)
        root_upload_file(File.join(templates_path, "nginx", ".htpasswd"), "/etc/nginx/#{application}.htpasswd")
      end
      sudo "ln -fs /etc/nginx/sites-available/#{application} /etc/nginx/sites-enabled/#{application}"
    end
  end
end
