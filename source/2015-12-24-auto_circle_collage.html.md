---
title: 全自動水玉コラ生成マシーン
date: 2015/12/24T23:59:59 +09:00
tags: processing
ogp:
  og:
    image:
      "": "/images/2015/12/24/google.png"
---

全自動水玉コラ生成マシーン
================================

聖夜なので表題のものを作った。

![demo](/images/2015/12/24/demo.gif)

[https://github.com/onk/auto_circle_collage](https://github.com/onk/auto_circle_collage)

processing で書いたアプリだけど、この記事の内容はほぼ OpenCV の話です。


仕組み
--------------------------------

1. **水着を自動認識**して「隠す」とマーク
2. **顔を自動認識**して「見せる」とマーク
3. マークに沿って**円充填**


水着領域の自動認識
--------------------------------

### 最初のアプローチ

* OpenCV を使って肌色認識
* 選択領域を膨張 -> 収縮させる
* 肌色との差分を取れば水着領域が完成


#### 肌色認識

先人が大量に居た。**RGB 色空間ではなく HSV 色空間を使う**というのがコツなようだ。

![HSV色空間](/images/2015/12/24/hsv.png)
[HSV色空間 - Wikipedia](https://ja.wikipedia.org/wiki/HSV%E8%89%B2%E7%A9%BA%E9%96%93)

HSV 色空間なら影になっている部分も抽出できる。

今回は Hue: 7..15 を肌色として定義した。

```java
PImage detectHada() {
  // 作業用に hue で grayscale にする
  opencv.loadImage(img);
  opencv.useColor(HSB);
  opencv.setGray(opencv.getH().clone());
  // 肌色は 7..15 と定義。inRange で取り出す
  opencv.inRange(7, 15);

  // ノイズ除去 MORPH_OPEN
  opencv.erode();
  opencv.dilate();

  return opencv.getSnapshot();
}
```

![影の部分も抽出できている様子](/images/2015/12/24/hada.png)


#### 膨張・収縮

欲しいのは肌色領域ではなく水着領域なので、肌と肌の間の空間を選択する必要がある。

膨張 -> 収縮し、最初との差分を取ることで取得できるのではないかと考えた。

![膨張・収縮で水着領域を選択](/images/2015/12/24/bouchou.png)

```java
// 輪郭を広げて戻す
for (int i = 0; i < 10; i++) { opencv.dilate(); }
for (int i = 0; i < 10; i++) { opencv.erode(); }
```

が、膨張する際に水着領域を上手に隠せないパターンが多い。

問題はループで 10 回回しているところにあり、本来は膨張・収縮時にパラメータを渡して解決すべきである。
デフォルトのパラメータのまま無理やりループで代替したところ、望むような膨張効果が得られていない。

opencv-processing でパラメータを渡す方法を調べるのに時間がかかりそうだったので次善の策に頼ることにした。


### 彩度の高いもの＝水着と仮定する

グラビアの水着はなぜか彩度の高いものが採用されていることが多いので「彩度の高いもの」という条件で水着が抽出できる。

いくつかパラメータを変えつつ試したが、

* Saturation: 180..255 (彩度の高いものを水着と仮定する)
* Brightness: 8..247 (あまりに暗いもの、明るすぎるものを除外する)

という絞り込みで、一定の条件の水着に対しては判定が効くようになった。

![彩度で水着判定](/images/2015/12/24/mizugi.png)

```java
void detectMizugi() {
  // 水着を彩度 180..255, 明度 8..247 と定義。inRange で取り出す
  opencv.loadImage(img);
  opencv.useColor(HSB);
  opencv.setGray(opencv.getS().clone());
  opencv.inRange(180, 255);
  PImage s = opencv.getSnapshot();

  opencv.loadImage(img);
  opencv.useColor(HSB);
  opencv.setGray(opencv.getB().clone());
  opencv.inRange(8, 247);
  PImage b = opencv.getSnapshot();

  // TODO: 無駄に PImage に一度変換して blend しているが
  // おそらく OpenCV のみで可能
  s.blend(b, 0, 0, width, height, 0, 0, width, height, MULTIPLY);

  opencv.loadImage(s);
  opencv.erode();
  opencv.dilate();

  ArrayList<Contour> contours = opencv.findContours();
  for (Contour contour : contours) {
    // 面積が小さすぎるものはノイズと判断して弾く
    if (contour.area() > 25) {
      mizugiAreaList.add(contour);
    }
  }
}
```

顔領域の自動認識
--------------------------------

OpenCV に任せて 2 行で解決した。

```java
opencv.loadCascade(OpenCV.CASCADE_FRONTALFACE);
Rectangle[] faces = opencv.detect();
```

![顔認識](/images/2015/12/24/face_detection.png)

デフォルトだと誤認識率が 30% ぐらいあるが、[OpenCV に同梱されている分類器を複数重ね合わせて使う](https://www.cyberagent.co.jp/technology/pdf/2011_12.pdf) のように、認識率を上げる手段がまだあるようだ。


円充填
--------------------------------

ここまでで「隠す領域」と「隠してはいけない領域」が抽出できたので、
いい感じに円で埋めていく作業をする。

* [【画像あり】水玉コラのコツ教える : 暇人＼(^o^)／速報 - ライブドアブログ](http://himasoku.com/archives/51904791.html)
* [ギークなフリーク　【水玉コラ】概論](http://yukiblg777.blog.fc2.com/blog-entry-22.html)

等で勉強したのだが、美しい水玉コラを作るには

* 水着を全て隠す
* 顔は必ず見せる
* 水玉は重ねない
* 水玉の数は多くても 15 個まで。少なければ少ないほど良い

といった標準的なルールの他に

* 穴が直線上に並ばないようにする
  * 隠された領域が水着っぽく見えてしまい、「らしさ」が薄れる
* 肩、くびれを見せる
  * ボディラインを強調することができ、より完成度が高く見える
* 谷間・アゴを見せる
  * 「らしさ」が一層高まる

といったテクニックがあるようだ。

とはいえ「くびれを学習させた教師データ」なるものは存在しないので、右上、左上から先に開けることでなるべく肩が含まれることを期待するにとどめた。

ここでゲームで培った当たり判定の技術 (OBB と点の当たり判定、凸包と点の当たり判定等) が生きたんだが、
処理を簡便化するために水着領域を凸包にしたことで谷間を見せることができなくなってしまった。

![凸包](/images/2015/12/24/totsuhou.png) (黄色の部分が当たり判定になるので谷間は必ず隠れてしまう)

ここは今後改善したい。


おまけ (曇りガラス効果)
--------------------------------

OpenCV には inpaint という演算があり、これを用いると「選択領域を取り除き、付近のピクセルを利用していい感じに埋める」ということができる。

と、いうことは「水着を取り除き」「ガウスボカし」で曇りガラス表現ができるのでは……！

![曇りガラス表現](/images/2015/12/24/omake.png)


まとめ
--------------------------------

OpenCV 超便利。

全自動で水玉を適用できるようになったので「動画に適用できるのでは？」とか「Chrome 拡張等で全 web ページに適用できる？」とか夢が広がっている。

また、人間が頭で考えている手順はだいたい自動化できるというのも再認識した。


偉大なる先人たち
--------------------------------

* 顔認識と肌色抽出及び効率的円配置による水玉コラの自動生成に関する研究
  * [あの人の研究所 - あの人の研究論文集 Vol.3 No.1](http://anohitolab.com/post/83494013472/%E3%81%82%E3%81%AE%E4%BA%BA%E3%81%AE%E7%A0%94%E7%A9%B6%E8%AB%96%E6%96%87%E9%9B%86-vol3-no1)
* [Python/OpenCVで顔検出を利用した肌色検出 - ぱろすけ's website](http://parosky.net/note/20131211) (現在リンク切れ)
* [水玉コラを動画でやってみたら予想以上に真っ裸になった． - ニコニコ動画:GINZA](http://www.nicovideo.jp/watch/sm20383995)
* [python + opencv で肌色を検出して褐色にするだけ - きよぽん＝サンのブログ](http://kiyopon.hatenablog.com/entry/2015/02/15/221706)
  * [python + opencv で肌色を検出して褐色にするだけ　その２ - きよぽん＝サンのブログ](http://kiyopon.hatenablog.com/entry/2015/02/19/011742)
  * [python + opencv で肌色を検出して褐色にするだけ　その3 - きよぽん＝サンのブログ](http://kiyopon.hatenablog.com/entry/2015/02/22/052328)
* [裸に見える水玉コラ作成アプリ「Strip-Her」](https://play.google.com/store/apps/details?id=com.gmail.surfrider2.applications.polkaGen)
  * [水玉コラアプリの中の人のブログ » 水玉コラを作れるAndroidアプリを作った。](http://surfrider2.dispatch-site.com/blog/2013/06/02/android%E3%82%A2%E3%83%97%E3%83%AA%E3%82%92%E4%BD%9C%E3%81%A3%E3%81%9F%E3%80%82/)
  * [水玉コラアプリの中の人のブログ » 画像から水着を除去して裸に見える画像を作る【Android/OpenCV】](http://surfrider2.dispatch-site.com/blog/2013/06/02/%E7%94%BB%E5%83%8F%E3%81%8B%E3%82%89%E6%B0%B4%E7%9D%80%E3%82%92%E9%99%A4%E5%8E%BB%E3%81%97%E3%81%A6%E8%A3%B8%E3%81%AB%E8%A6%8B%E3%81%88%E3%82%8B%E7%94%BB%E5%83%8F%E3%82%92%E4%BD%9C%E3%82%8B%E3%80%90an/)
