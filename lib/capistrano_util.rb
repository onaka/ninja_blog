module Capistrano
  module Util
    def upload_file(local_path, remote_path)
      info "Uploading: #{local_path} -> #{remote_path}"
      tmpfile_path = __create_remote_temp_file(local_path, remote_path)
      execute "mv #{tmpfile_path} #{remote_path}"
      execute "chmod a+r #{remote_path}"
    end

    # root権限のファイルでも上書きして、root権限を付ける
    def root_upload_file(local_path, remote_path)
      info "Uploading: #{local_path} -> #{remote_path}"
      tmpfile_path = __create_remote_temp_file(local_path, remote_path)

      sudo "mv #{tmpfile_path} #{remote_path}"
      sudo "chown root:root #{remote_path}"
      sudo "chmod a+r #{remote_path}"
    end

    def root_backup_file(file)
      backup_file = file + ".#{Time.now.strftime("%Y%m%d%H%M%S")}"
      if test("[ -f #{file} ]")
        sudo "cp -a #{file} #{backup_file}"
      else
        info "Backup aborted! File does not exist: #{file}."
      end
    end

    # ログの書き込みやログローテができるディレクトリを作る
    def create_writable_dir(dir)
      create_dir(dir, mode: 775, owner: fetch(:www_user), group: fetch(:www_group))
    end

    def create_dir(dir, options = {})
      sudo "mkdir -p #{dir}"
      sudo "chmod #{options[:mode]} #{dir}" if options[:mode]
      sudo "chown -R #{options[:owner]} #{dir}" if options[:owner]
      sudo "chgrp -R #{options[:group]} #{dir}" if options[:group]
    end

    # task の記述簡略化のため
    def application
      fetch(:application)
    end

    private

      def __create_remote_temp_file(local_path, remote_path)
        # capが /tmp/#{:application} にディレクトリを生成する関係でアプリ名と同名のファイル（logrotateの設定ファイルとか）を
        # アップしようとすると失敗するため、1つ階層を深くする
        _user = fetch(:user, `whoami`.chomp)
        tmpdir_path = File.join(fetch(:tmp_dir), _user)
        execute "mkdir -p #{tmpdir_path}"

        file = ERB.new(File.read(local_path))
        contents = StringIO.new(file.result(binding))
        tmpfile_path = "#{tmpdir_path}/#{File.basename(remote_path)}"
        upload! contents, tmpfile_path
        tmpfile_path
      end

      def templates_path
        "config/deploy/templates"
      end
  end
end
